;
; Spherical harmonics transformation
;
; INPUT:
; qq: variable in real space [jx,kx]
; y: colatitude from 0. to pi [jx]
; z: longitude from -pi to pi [kx]
;
; OUTPUT:
; ffqq: complex [kx/4+1,kx/4+1]
FUNCTION sht,qq,y,z  
  jx = n_elements(y)
  kx = n_elements(z)
  
  dy = y[1] - y[0]
  dz = z[1] - z[0]
  lmax = kx/4
  
  fqq0 = fft(qq,dimension=2)
  fqq = complexarr(jx,kx/2+1)
  fqq[*,0] = fqq0[*,0]*2.d0*!pi
  for k = 1,kx/2-1 do begin
     fqq[*,k] = (fqq0[*,k] + conj(fqq0[*,kx-k]))*2.d0*!pi
  endfor
  fqq[*,kx/2] = fqq0[*,kx/2]*2.d0*!pi
  ffqq = complexarr(lmax+1,lmax+1)

  for m = 0,lmax do begin
     fm = float(m)
     if m eq 0 then begin
        pm = 1.d0/sqrt(4.d0*!pi)
     endif else begin
        pm = -sqrt((2.*fm+1.)/(2.*fm))*sin(y)*pm
        pm[where(abs(pm) le 1.d-100)] = 0.d0
     endelse

     ffqq[m,m] = total(fqq[*,m]*pm*sin(y)*dy)
     
     pm1 = pm
     pm2 = 0.d0
     for l = m+1,lmax do begin
        fl = float(l)
        
        faca = sqrt((2.*fl-1.)*(2.*fl+1.)/(fl+fm)/(fl-fm))
        facb = sqrt( $
               (2.*fl+1.)/(2.*fl-3.)* $
               (fl + fm - 1.)*(fl - fm - 1.)/(fl + fm)/(fl - fm) $
                   )
        pm0 = faca*cos(y)*pm1 - facb*pm2
        ffqq[l,m] = total(fqq[*,m]*pm0*sin(y)*dy)
        
        pm2 = pm1
        pm1 = pm0
     endfor
  endfor
  
  return,ffqq
end
