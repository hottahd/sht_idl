;;Progmram
loadct,39
jx = 128L
kx = 256L

ymin = 0
ymax = !pi
dy = (ymax-ymin)/double(jx)
y = ymin + (0.5d0+findgen(jx))*dy
yy = y#replicate(1.,kx)

zmin = -!pi
zmax = !pi
dz = (zmax-zmin)/double(kx)
z = zmin + (0.5d0+findgen(kx))*dz
zz = replicate(1.,jx)#z

qq = 1.d0/sqrt(4.d0*!pi)*replicate(1.,jx,kx) ;; l=0, m=0
;qq = sqrt(3.d0/4.d0 /!pi)*cos(yy) ;; l=1, m=0
;qq = sqrt(5.d0/16.d0/!pi)*sin(yy)*cos(zz) ;; l=1, m=1
;qq = sqrt(5.  /16.d0/!pi)*(3.*cos(yy)^2-1.d0) ;; l=2, m=0
;qq = sqrt(15./8./!pi)*sin(yy)*cos(yy)*cos(zz) ;; l=2, m=1
;qq = sqrt(15. /32.d0/!pi)*sin(yy)^2*cos(2.*zz) ;; l=2, m=2
;qq = sqrt(7./16.d0/!pi)*(5.*cos(yy)^3 - 3.*cos(yy)) ;; l=3, m=0
;qq = sqrt(21./64.d0/!pi)*sin(yy)*(5.*cos(yy)^2 - 1.)*sin(zz) ;; l=3, m=1
;qq = sqrt(105./32.d0/!pi)*sin(yy)^2*cos(yy)*sin(2.*zz) ;; l=3, m=2
qq = sqrt(35./64./!pi)*sin(yy)^3*sin(3.*zz) ;; l=3, m=3
;qq = 1.d0/1024.d0*sqrt(969969.d0/!pi)*sin(yy)^10*cos(10.d0*zz) ;; l=10,m=10

fqq = sht(qq,y,z)

end
